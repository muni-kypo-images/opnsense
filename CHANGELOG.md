# Changelog

## [qemu-0.4.0] - 2024-07-25
### Changed
- Update OPNsense to 24.7

## [qemu-0.3.0] - 2024-05-16
### Changed
- Update OPNsense to 24.1

## [qemu-0.2.0] - 2023-03-20
### Changed
- Update OPNsense to 23.1

## [qemu-0.1.0] - 2021-07-31
### Changed
- First QEMU version, although with known issues

[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/opnsense/-/tree/qemu-0.1.0
[qemu-0.2.0]: https://gitlab.ics.muni.cz/muni-kypo-images/opnsense/-/tree/qemu-0.2.0
[qemu-0.3.0]: https://gitlab.ics.muni.cz/muni-kypo-images/opnsense/-/tree/qemu-0.3.0
[qemu-0.4.0]: https://gitlab.ics.muni.cz/muni-kypo-images/opnsense/-/tree/qemu-0.4.0
